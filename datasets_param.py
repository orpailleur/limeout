german = {
    "source_name" : "datasets/german.data",
    "sep" : " ",
    "all_categorical_features" : [0,2,3,5,6,8,9,11,13,14,16,18,19],
    "sensitive" : [8,18,19], # statussex, telephone, foreignworker
    "priv_group" : {
        "statussex" : 2, # A93 male single
        "telephone" : 1, # A192 : yes, registered under the customers name
        "foreignworker" : 0 # A202 : no
        },
    "pos_label" : 0
    }
    
adult = {
    "source_name" : "datasets/adult.data",
    "sep" : ",",
    "all_categorical_features" : [1, 3, 5, 6, 7, 8, 9, 13],
    "sensitive" : [5, 8, 9],
    "priv_group" : {
        "MaritalStatus" : 0, # Married-civ-spouse
        "Race" : 0, # white 
        "Sex" : 1 # male
        },
    "pos_label" : 1
    }

hdma = {
    "source_name" : "datasets/hmda2.data",
    "sep" : ";",
    "all_categorical_features" : [0,1,2,3,5,15,17,19,24,25,26,27,28],
#     "all_categorical_features" : [0,1,2,3,5,10,12,13,14,19,20], # 15,16
    "sensitive" : [1,2,3], # "derived_race","derived_sex","derived_ethnicity"
    "priv_group" : {
        "derived_sex" : 1, # Male ('Female', 'Male')
        "derived_race" : 6, # White  ('Black or African American', 'White', 'Asian', 'Native Hawaiian or Other Pacific Islander', 'American Indian or Alaska Native', '2 or more minority races', 'Race Not Available')
        "derived_ethnicity" : 0 # Ethnicity Not Available ('Ethnicity Not Available', 'Hispanic or Latino', 'Joint', 'Not Hispanic or Latino')
        },
    "pos_label" : 1 # high-priced
    }
    
lsac = {
    "source_name" : "datasets/lsac.data",
    "sep" : ";",
    "all_categorical_features" : [3,4,5],
    "sensitive" : [4,5],
    "priv_group" : {
        "sex" : 1, # "Male" (0 means Female)
        "race" : 2 # "White" (0 means Black and 1 means "Other")
        },
    "pos_label" : 1 # Passed (0 is Failed_or_not_attempted) 
    }

default = {
    "source_name" : "datasets/default.data",
    "sep" : ";",
    "all_categorical_features" : [1,2,3],
    "sensitive" : [1,3], # sex, marriage,
    "priv_group" : {
        "SEX" : 0, # "1" male (X2: Gender (1 = male; 2 = female).)
        "MARRIAGE" : 2 # "2" single  (X4: Marital status (1 = married; 2 = single; 3 = others).)
        },
    "pos_label" : 1 # This research employed a binary variable, default payment (Yes = 1, No = 0), as the response variable
    }
