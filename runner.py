import argparse
import datetime

from sklearn.ensemble import AdaBoostClassifier
from sklearn.ensemble._bagging import BaggingClassifier
from sklearn.ensemble._forest import RandomForestClassifier
from sklearn.ensemble._gb import GradientBoostingClassifier
from sklearn.linear_model._logistic import LogisticRegression
from sklearn.mixture._gaussian_mixture import GaussianMixture
from sklearn.neural_network._multilayer_perceptron import MLPClassifier
from sklearn.svm._classes import SVC

from core import LimeOut
import lime_global
from sklearn.tree import DecisionTreeClassifier


def main(source_name, sep, train_size, to_drop, all_categorical_features, max_features, algo, exp, sampling_size):
       
    limeout = LimeOut(source_name, sep, train_size, to_drop, all_categorical_features, algo, exp, max_features, sampling_size)   
    
    is_fair, _, accuracy = limeout.is_fair()
         
    if not is_fair:
        is_fair, _,  accuracy = limeout.ensemble_out()
        return limeout.ensemble
     
    else:
        return limeout.original_model    

def algo_parser(algo_str):
    
    algo = algo_str.lower()
    
    if algo == "mlp":
        return MLPClassifier
    elif algo == "logreg":
        return LogisticRegression
    elif algo == "rf":
        return RandomForestClassifier
    elif algo == "ada":
        return AdaBoostClassifier
    elif algo == "bagging":
        return BaggingClassifier
    elif algo == "gaussianmixture":
        return GaussianMixture
    elif algo == "gradient":
        return GradientBoostingClassifier
    elif algo == "svm":
        return SVC
    elif algo == "dt":
        return DecisionTreeClassifier
    else:
        return None
    
def exp_parser(algo_str):
    
    algo = algo_str.lower()
    
    if algo == "lime":
        return lime_global.fairness_eval
    else:
        return None

   
if __name__ == "__main__":
    
    parser = argparse.ArgumentParser(description='ExpOut: process fairness for classification')
    parser.add_argument('--data')  
    parser.add_argument('--trainsize', type=float)
    parser.add_argument('--algo')  
    parser.add_argument('--exp')  
    parser.add_argument('--samplesize', type=int)  
    parser.add_argument('--max_features', type=int)  
    parser.add_argument('-cat_features', '--cat_features', action='store', dest='cat_features_list', type=int, nargs='*', default=[], help="Examples: -i ")
    parser.add_argument('-drop', '--drop', action='store', dest='drop_list', type=int, nargs='*', default=[], help="Examples: -i ")
    parser.add_argument('--sep')
#     parser.add_argument('--fairecriteria', type=int)
    
    args = parser.parse_args() 
    
    now = datetime.datetime.now()
    print(now.year,'-', now.month,'-', now.day,',', now.hour,':', now.minute,':', now.second,sep='')
    
    main(args.data, args.sep, args.trainsize, args.drop_list, args.cat_features_list, args.max_features, algo_parser(args.algo), exp_parser(args.exp), args.samplesize)#, args.fairecriteria)   