# LimeOut
The project aims to tackle process fairness for Machine Learning Models. The key idea is to use an explanation method, namely, LIME (as implemented [here](https://github.com/marcotcr/lime/)) to assess whether a given classifier was fair by measuring its reliance on salient or sensitive features, and that is then integrated in a human-centered workflow called LimeOut,  that receives as input a triple _(M,D,F)_ of a classifier _M_, a dataset _D_ and a set _F_ of sensitive features, and outputs a classifier _M<sub>final</sub>_ less dependent on sensitive features without compromising accuracy. To  achieve  both  goals, LimeOut  relies  on feature dropout to produce a pool of classifiers that are then combined through an ensemble approach. Feature dropout receives a classifier and a feature _a_ as input, and produces a classifier that does not take _a_ into account.

LimeOut is based on the work presented in these papers: [[1]](https://arxiv.org/abs/2006.10531) and [[2]](https://hal.archives-ouvertes.fr/hal-02864059v5). 

# Experiments
We implemented this approach and we make use of a larger family of ML classifiers (that include AdaBoost, Bagging, Random Forest, and Logistic Regression). We evaluate LimeOut's output classifiers with respect to a wide variety fairness metrics on many other datasets (e.g., Adult, German Credit Score, HDMA dataset, Taiwanese Credit Card dataset, LSAC).

## Dependencies
* Python >= 3.7
* Scikit-learn >= 0.23.1 