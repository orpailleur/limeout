import datetime

from core import LimeOut, evaluation, Model, fairness_metrics_eval, \
    evaluation_fixed_threshold
from datasets_param import *

import numpy as np
import pandas as pd
from runner import algo_parser, exp_parser


def one_experiment(source_name, sep, train_size, to_drop, all_categorical_features, max_features, algo, exp, sampling_size, threshold_exp, p_groups, p_label):
    
    original_f_metrics = {}
    ensemble_f_metrics = {}
    
    for i in range(0,10):
        limeout = LimeOut(source_name, sep, train_size, to_drop, all_categorical_features, algo, exp, max_features, sampling_size, seed=i, threshold=threshold_exp)   
        
        actual_sensitive, is_fair1, contributions_original, accuracy_original, threshold_orig = limeout.is_fair()
        print("~~~~~~~~~\nOriginal model DONE\n~~~~~~~~~")
        if not is_fair1:
            is_fair2, contributions_ensemble, accuracy_ensemble, threshold_ens = limeout.ensemble_out(actual_sensitive)
            print("Ensemble model DONE\n~~~~~~~~~")
        else:
            print("Original model is FAIR")
        print("~~~~~~~~~")
            
        print("#########\nRESULTS",i,"BEGIN\n#########")
        print("Original model accuracy", accuracy_original, threshold_orig)
        print(contributions_original)

        if is_fair1:
            continue
             
        if not is_fair1:
            print("Ensemble model accuracy", accuracy_ensemble, threshold_ens)
            ensemble = limeout.ensemble
            actual_sensitive_indexes = [i for i,_ in actual_sensitive]
            for f in range(len(actual_sensitive_indexes)):
                submodel = Model([ensemble.models[f]],[ensemble.encoders[f]],[actual_sensitive_indexes[f]])
#                 accuracy, threshold = evaluation(submodel.prob(limeout.test), limeout.labels_test)
                accuracy = evaluation_fixed_threshold(submodel.prob(limeout.test), limeout.labels_test, threshold_ens)
                print("Ensemble Submodel accuracy", accuracy, threshold_ens)
            submodel = Model([ensemble.models[len(actual_sensitive_indexes)]],[ensemble.encoders[len(actual_sensitive_indexes)]],[actual_sensitive_indexes])
#             accuracy = evaluation_fixed_threshold(submodel.prob(limeout.test), limeout.labels_test,threshold_orig)
            accuracy, threshold = evaluation(submodel.prob(limeout.test), limeout.labels_test)
            print("Ensemble Submodel accuracy", accuracy, threshold)
                    
            print(contributions_ensemble)
            print("#########\nRESULTS",i,"END\n#########")
         
        #############
         
        y = np.array([[value] for value in limeout.labels_test])
        res = np.concatenate((y,limeout.test[:,actual_sensitive_indexes]),axis=1)
         
        sensitive_features_names = list(np.array(limeout.feature_names)[actual_sensitive_indexes])
         
        true_labels = pd.DataFrame(res,columns=['y_true']+sensitive_features_names)
        true_labels.set_index(sensitive_features_names, inplace=True)
 
        print("\n\n****************\nFAIRNESS METRICS\n****************")
        for sen_feature in sensitive_features_names:
            dp,eq,ea,aod,fpr_diff,di = fairness_metrics_eval(limeout.original_model.prob(limeout.test), true_labels, sen_feature, p_groups[str(sen_feature)], p_label)
               
            if sen_feature not in original_f_metrics:
                res = []
                original_f_metrics[sen_feature] = res
            original_f_metrics[sen_feature].append((dp,eq,ea,aod,fpr_diff,di))
               
        for sen_feature in sensitive_features_names:
            dp,eq,ea,aod,fpr_diff,di = fairness_metrics_eval(limeout.ensemble.prob(limeout.test), true_labels, sen_feature, p_groups[sen_feature], p_label)
               
            if sen_feature not in ensemble_f_metrics:
                res = []
                ensemble_f_metrics[sen_feature] = res
            ensemble_f_metrics[sen_feature].append((dp,eq,ea,aod,fpr_diff,di))
           
    print("ORIGINAL MODEL")
    for k,v in original_f_metrics.items():
        dp_l,eq_l,ea_l,eo_l,fpr_diff_l,di_l = [dp for dp,_,_,_,_,_ in v], [eq for _,eq,_,_,_,_ in v], [ea for _,_,ea,_,_,_ in v], [eo for _,_,_,eo,_,_ in v], [fpr_diff for _,_,_,_,fpr_diff,_ in v], [di for _,_,_,_,_,di in v]
        print("Sensitive Feature:", k)
        print(np.mean(dp_l),np.mean(eq_l),np.mean(ea_l),np.mean(eo_l),np.mean(fpr_diff_l),np.mean(di_l))
        print(np.std(dp_l),np.std(eq_l),np.std(ea_l),np.std(eo_l),np.std(fpr_diff_l),np.std(di_l))
    print("FIXOUT ENSEMBLE MODEL")
    for k,v in ensemble_f_metrics.items():
        dp_l,eq_l,ea_l,eo_l,fpr_diff_l,di_l = [dp for dp,_,_,_,_,_ in v], [eq for _,eq,_,_,_,_ in v], [ea for _,_,ea,_,_,_ in v], [eo for _,_,_,eo,_,_ in v], [fpr_diff for _,_,_,_,fpr_diff,_ in v], [di for _,_,_,_,_,di in v]
        print("Sensitive Feature:", k)
        print(np.mean(dp_l),np.mean(eq_l),np.mean(ea_l),np.mean(eo_l),np.mean(fpr_diff_l),np.mean(di_l))
        print(np.std(dp_l),np.std(eq_l),np.std(ea_l),np.std(eo_l),np.std(fpr_diff_l),np.std(di_l))

    
def main(general_param, data_param):
#     for t in [0.1,0.2,0.3,0.4,0.5]:
#         print('threshold =',t)
#         param["threshold"] = t
    one_experiment(data_param["source_name"], data_param["sep"], general_param["train_size"], data_param["sensitive"], data_param["all_categorical_features"], general_param["max_features"], algo_parser(general_param["algo"]), exp_parser(general_param["exp"]), general_param["sample_size"], general_param["threshold"], data_param['priv_group'], data_param['pos_label'])


general_parameters = {
    "train_size" : 0.7,
    "algo" : "logreg",
    "exp" : "lime",
    "max_features" : 10,
    "sample_size" : 500,
    "threshold" : None
    }


if __name__ == "__main__":
    
    dataset_param = german
    
    now = datetime.datetime.now()
    print(now.year,'-', now.month,'-', now.day,',', now.hour,':', now.minute,':', now.second,sep='')
    print("Data Parameters:",dataset_param)
    print("General Parameters:",general_parameters)
    main(general_parameters, dataset_param)
    